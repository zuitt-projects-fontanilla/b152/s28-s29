/*

	First, we load the expressjs module into our application and saved
	it in a variable called express.

*/


const express = require("express");

/* 
	Create an application with expressjs
	This creates an application that uses express and stores it as an app
	app is our server
*/
const app = express();
//port is a var to contain the number we want to designate to our server
const port = 4000;



//express.json() is a method from express which allow us to handle the streaming
//of data and automatically parse the incoming JSON from our request's body



app.use(express.json());

//mock data
let users = [
	
	{
		username: "tinaRCBC",
		email: "tinaLRCBC@gmail.com",
		password: "tinaRCBC990"
	},

	{
		username: "gwenstacy1",
		email: "spidergwen@gmail.com",
		password: "gwenOGspider"
	}
];


let items = [

	{
		name: "Stick-O",
		price: 70,
		isActive: true
	},
	{
		name: "Doritos",
		price: 150,
		isActive: true
	},


];





//app.list() allows us to designated the server to run on the indicated port
//number and run console log afterwards

//app.get(<endpoint>,<function handling request and response>)
app.get('/',(req,res)=>{


	//Once the route is accessed, we can send a responsed with the use of 
	//res.send()
	//res.send() actually combines our writeHead() and end().
	//it is used to send a response to the client and ends the response.
	res.send("Hello World from our first ExpressJS API");
})

app.get('/hello',(req,res)=>{

	res.send("Hello from Batch 152!");
})

app.get('/users',(req,res)=>{

	res.send(users);
})

app.post('/users',(req,res)=>{

	//With the use of express.json(), we simply have to access the body
	//property of our request object to get the body/input of the request.
	//The receiving of data and the parsing of JSON to JS object has already been 
	//done by express.json()
	//Note: When dealing with a rout that receives data from a request body 
	//is a good practice to chec the incoming data from the client.
	console.log(req.body);

	let newUser = {
		username: req.body.username,
		email: req.body.email,
		password: req.body.password
	}

	users.push(newUser);
	console.log(users);

	res.send(users);
})


//delete user route
app.delete('/users',(req,res)=>{

	users.pop();
	console.log(users);

	res.send(users)
})


app.put('/users/:index',(req,res)=>{

	console.log(req.body);
	//object whic contains the value of the url params
	//url params is captured by route parameter (:parameterName)
	console.log(req.params)
	/*users[req.body.index].password = req.body.password

	res.send(users[req.body.index])*/
	let index = parseInt(req.params.index);

	users[index].password = req.body.password;
	//send the updated user to the client:
	//provide the index variable to be the index for the particular item n the array
	res.send(users[index])
})


//GET method requests should not have a request body. It may
//have headers for additional info. We can pass small amount of data 
//from our url.

//Route params are values we can pass via the url.
//This is done specifically to allow us to send smallamount of data 
//into our server through the request URL.
//Route parameters can be defined in the endpoint of a route with 
//:parameterName

app.get('/users/getSingleUser/:index',(req,res)=>{

	console.log(req.params);
	//parseInt the value of the number coming from req.params

/*
	{ index" '0'}
*/

//parseInt() the value of req.params.index to convert the value from stirng 
//to number.

	let index = parseInt(req.params.index);
	console.log(number);

//send the user with the appropriate index number
	res.send(users[index])

})



app.get('/items',(req,res)=>{

	res.send(items);

})

app.post('/items',(req,res)=>{

	console.log(req.body);

	let newItem = {

		name: req.body.name,
		price: req.body.price,
		isActive: req.body.isActive
	}

	items.push(newItem);
	console.log(items);

	res.send(items);


})

app.put('/items/:index',(req,res)=>{

	console.log(req.body);
	console.log(req.params);

	let index = parseInt(req.params.index);

	items[index].price = req.body.price;

	res.send(items[index])
	console.log(items);

})





app.put('/items',(req,res)=>{

	console.log(req.body);

	items[req.body.index].price = req.body.price

	res.send(items[req.body.index])


})




app.get('/items/getSingleItem/:index', (req,res)=>{


	console.log(req.params);

	let index = parseInt(req.params.index);
	console.log(index);

	res.send(users[index])

});


app.put('/items/archive/:index', (req,res)=>{

	
	console.log(req.params);

	let index = parseInt(req.params.index);

	items[index].isActive = false;

	res.send(items[index]);
	
})


app.put('/items/activate/:index', (req,res)=>{

	
	console.log(req.params);

	let index = parseInt(req.params.index);

	items[index].isActive = true;

	res.send(items[index]);
	
})







app.listen(port,()=>console.log(`Server is running at port ${port}`));








